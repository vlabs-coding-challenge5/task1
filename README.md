
Still in development stage. The the website takes the number of rows as an input through the dropdown menu and creates a table.
The second half of the problem is yet to be completed. 


**Create a dynamic table as mentioned below   :**

- The user has to select the number of rows required from a **dropdown list**. Based on the selection the rows should generate dynamically in the table. The dropdown list should contain rows starting from 2 to 10.
- There will be 2 columns, where:
1. First Column : The user can enter numbers (float) in the textboxes.
2. Second column : Using a button click, calculate the cube of the numbers provided in the first column. Thereafter, display the calculated cube values in the textbox in the same row of the second column.
3. Include a validation step in the code. If the user has not entered the number, display the relevant error message to the user in the best possible way.
- Instructions for this task
- Use CSS to provide rich experience and **mobile responsiveness.**
- Use HTML, CSS and TypeScript only (JavaScript files to be created from TypeScript files).
